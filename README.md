# Fatpig project
## Introduction
 - This is the internal project with code "FatPig"
 - The project is powered by [Play Framework]
 - The project use [Polymer] (a [Web Component] technology) as web technology.
 - The project use [Cassandar] (a [NoSql] database) as storage
 - One of the main goal of this project is to provide sample implementation and guidelines for LeanDev's future project
 - As a guest, you are free to folk it and do any experiment
 - Do not merge to develop!

## How to set it up
 - To be finished later
 - Original test code written in registry-web, can be accessed at http://localhost:9000/registry/testepoly

## Submodules

### Web Modules
 - [Registry Web]
 - [Bonus Web]
 - [Work time Web]

### Business Models
 - [Registry Model]
 - [Bonus Model]
 - [Work time Model]

### Adaptors
 - [Bonus-Registry Adaptor]
 - [Work time-Registry Adaptor]

### Supporting Modules
 - [API Data]  API for data shared among different business modules
 - [Database Client]  Database client used to access an NoSql database, now has implementation access to Cassendar, but the module is designed to work with any type of NoSql DB.


[Registry Web]:modules/fatpig-registry-web/README.md
[Bonus Web]:modules/fatpig-bonus-web/README.md
[Work time Web]:modules/fatpig-worktime-web/README.md
[Registry Model]:modules/fatpig-registry-model/README.md
[Bonus Model]:modules/fatpig-bonus-model/README.md
[Work time Model]:modules/fatpig-worktime-model/README.md

[Play Framework]:http://playframework.com/
[Polymer]:http://polymer-project.org
[Web Component]:http://webcomponents.org
[Cassandar]:http://cassandra.apache.org
[NoSql]:http://nosql-database.org