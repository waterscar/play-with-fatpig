import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import play.GlobalSettings;
import play.Logger;
import play.libs.Json;

/**
 * @author Edward Xie
 *         15/1/30.
 */
@SuppressWarnings("UnusedDeclaration")
public class Global extends GlobalSettings {

    @Override
    public void onStart(play.Application application) {
        //not needed since we decide to move to Angular from Ember
//        ObjectMapper objectMapper=new ObjectMapper();
//        objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE,true);
//        Json.setObjectMapper(objectMapper);
        Logger.info("Fatpig application has started, Global setting configured");
    }

}
