name := """play-with-fatpig"""

lazy val commonSettings = Seq(
  organization := "se.leandev",
  version := "1.0-SNAPSHOT",
  scalaVersion := "2.11.1"
)

lazy val sharedDependencies = Seq(
  libraryDependencies ++= Seq(
    // Uncomment to use Akka
    //"com.typesafe.akka" % "akka-actor_2.11" % "2.3.6",
    "junit" % "junit" % "4.11" % "test",
    "com.novocode" % "junit-interface" % "0.10" % "test"
  )
)

lazy val playWebDependencies = Seq(
  libraryDependencies ++= Seq(
    cache
  )
)

lazy val root = (project in file(".")).enablePlugins(PlayJava).settings(commonSettings: _*)
  .settings(playWebDependencies: _*).settings(sharedDependencies: _*)
  .dependsOn(registryWeb, bonusWeb, worktimeWeb).aggregate(registryWeb, bonusWeb, worktimeWeb)

//business modules
lazy val registryWeb = (project in file("modules/fatpig-registry-web")).enablePlugins(PlayJava).settings(commonSettings: _*)
  .settings(playWebDependencies: _*).settings(sharedDependencies: _*)
  .dependsOn(registryModel).aggregate(registryModel)
lazy val registryModel = (project in file("modules/fatpig-registry-model")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)
  .dependsOn(apiData)


lazy val bonusWeb = (project in file("modules/fatpig-bonus-web")).enablePlugins(PlayJava).settings(commonSettings: _*)
  .settings(playWebDependencies: _*).settings(sharedDependencies: _*)
  .dependsOn(bonusModel, bonusRegistryAdaptor)
  .aggregate(bonusModel, bonusRegistryAdaptor)
lazy val bonusModel = (project in file("modules/fatpig-bonus-model")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)
  .dependsOn(apiData)

lazy val worktimeWeb=(project in file("modules/fatpig-worktime-web")).enablePlugins(PlayJava).settings(commonSettings: _*)
  .settings(playWebDependencies: _*).settings(sharedDependencies: _*)
  .dependsOn(worktimeModel,worktimeRegistryAdaptor)
  .aggregate(worktimeModel,worktimeRegistryAdaptor)
lazy val worktimeModel=(project in file("modules/fatpig-worktime-model")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)
  .dependsOn(apiData)


//adaptors
lazy val bonusRegistryAdaptor = (project in file("modules/fatpig-bonus-registry-adaptor")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)
  .dependsOn(bonusModel, registryModel)

lazy val worktimeRegistryAdaptor = (project in file("modules/fatpig-worktime-registry-adaptor")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)
  .dependsOn(registryModel, worktimeModel, apiData)


//shared components
lazy val apiData = (project in file("modules/fatpig-api-data")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)
lazy val cassandraData = (project in file("modules/fatpig-cassandra-client")).settings(commonSettings: _*)
  .settings(sharedDependencies: _*)




