package se.leandev.fatpig.worktime.data;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class VacationServiceTest {

    @Test
    public void testGetVacationListForEmployeeById() throws
                                                     Exception {
        List<Vacation> vacationList=new VacationService().getVacationListForEmployeeById(1);
        assertEquals(5,vacationList.size());
    }
}