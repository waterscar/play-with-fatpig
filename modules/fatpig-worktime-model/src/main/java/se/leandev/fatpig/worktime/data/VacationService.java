package se.leandev.fatpig.worktime.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class VacationService {
    public List<Vacation> getVacationListForEmployeeById(int id){
        return createSampleVacationList();
    }

    private List<Vacation> createSampleVacationList() {
        List<Vacation> vacations=new ArrayList<>();
        vacations.add(createSampleVacation(1,"13-01-01", BigDecimal.valueOf(8)));
        vacations.add(createSampleVacation(2,"13-01-02", BigDecimal.valueOf(8)));
        vacations.add(createSampleVacation(3,"13-01-03", BigDecimal.valueOf(8)));
        vacations.add(createSampleVacation(4,"13-01-04", BigDecimal.valueOf(8)));
        vacations.add(createSampleVacation(5,"13-01-05", BigDecimal.valueOf(8)));
        return vacations;
    }

    private Vacation createSampleVacation(int id, String date, BigDecimal hour) {
        Vacation vacation=new Vacation();
        vacation.setDate(date);
        vacation.setId(id);
        vacation.setHours(hour);
        return vacation;
    }
}
