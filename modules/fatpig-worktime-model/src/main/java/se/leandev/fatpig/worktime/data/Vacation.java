package se.leandev.fatpig.worktime.data;

import java.math.BigDecimal;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class Vacation {
    private int id;
    private String date;
    private BigDecimal hours;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getHours() {
        return hours;
    }

    public void setHours(BigDecimal hours) {
        this.hours = hours;
    }
}
