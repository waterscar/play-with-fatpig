package se.leandev.fatpig.worktime.data;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class VacationPlan {
    private int id;
    private EmployeeGeneralInfo employee;

    private List<Vacation> vacationList=new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EmployeeGeneralInfo getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeGeneralInfo employee) {
        this.employee = employee;
    }

    public List<Vacation> getVacationList() {
        return vacationList;
    }

    public void setVacationList(List<Vacation> vacationList) {
        this.vacationList = vacationList;
    }
}
