package controllers.registry;

import play.mvc.*;
import views.html.registryView.index;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Registry Module"));
    }

}
