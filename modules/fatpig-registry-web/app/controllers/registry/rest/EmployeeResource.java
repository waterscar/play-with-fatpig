package controllers.registry.rest;

import play.libs.Json;
import play.mvc.*;
import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;
import se.leandev.fatpig.registry.data.EmployeeService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Edward Xie
 *         15/2/4.
 */
public class EmployeeResource extends Controller{
    //todo-ed These sample method need to be removed and replaced by real CRUD

    public static Result getSampleEmployeeGeneralInfo(){
        EmployeeService employeeService=new EmployeeService();

        EmployeeGeneralInfo employeeToSend=employeeService.getSampleEmployee(10,"Edward","Xie","EX@leandev.se");
        EmployeeGeneralInfo employeeToReceive=employeeService.getSampleEmployee(11,"Billy","Xiong","BX@leandev.se");
        List<EmployeeGeneralInfo> employeeGeneralInfos=employeeService.getSampleEmployeeGeneralInfoList();
        employeeGeneralInfos.add(employeeToSend);
        employeeGeneralInfos.add(employeeToReceive);

        return ok(Json.toJson(employeeGeneralInfos));
    }
}
