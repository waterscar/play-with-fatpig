package se.leandev.fatpig.bonusregistry.adaptor;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;
import se.leandev.fatpig.bonus.data.BonusTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class EmployeeBonus {

    private EmployeeGeneralInfo employeeGeneralInfo;
    private List<BonusTransaction> bonusReceived=new ArrayList<>();
    private List<BonusTransaction> bonusGiven=new ArrayList<>();

    public EmployeeGeneralInfo getEmployeeGeneralInfo() {
        return employeeGeneralInfo;
    }
    public void setEmployeeGeneralInfo(EmployeeGeneralInfo employeeGeneralInfo) {
        this.employeeGeneralInfo = employeeGeneralInfo;
    }

    public List<BonusTransaction> getBonusReceived() {
        return bonusReceived;
    }

    public void setBonusReceived(List<BonusTransaction> bonusReceived) {
        this.bonusReceived = bonusReceived;
    }

    public List<BonusTransaction> getBonusGiven() {
        return bonusGiven;
    }

    public void setBonusGiven(List<BonusTransaction> bonusGiven) {
        this.bonusGiven = bonusGiven;
    }
}
