package se.leandev.fatpig.bonusregistry.adaptor;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;
import se.leandev.fatpig.bonus.data.BonusTransaction;
import se.leandev.fatpig.bonus.data.BonusTransactionService;
import se.leandev.fatpig.registry.data.EmployeeService;

import java.util.List;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class EmployeeBonusAdaptor {
    private BonusTransactionService bonusTransactionService = new BonusTransactionService();
    private EmployeeService employeeService=new EmployeeService();

    public EmployeeBonus getSampleEmployeeBonus() {
        EmployeeBonus employeeBonus = new EmployeeBonus();
        BonusTransactionService bonusTransactionService = new BonusTransactionService();
        List<EmployeeGeneralInfo> employeeGeneralInfos = getEmployeeGeneralInfoFromEmployeeModule();
        EmployeeGeneralInfo employeeToSend = employeeGeneralInfos.get(0);
        EmployeeGeneralInfo employeeToReceive = employeeGeneralInfos.get(1);

        employeeBonus.setEmployeeGeneralInfo(employeeToSend);

        List<BonusTransaction> sampleTrsnactions = bonusTransactionService.getSampleBonusTransaction();
        for (BonusTransaction sampleTrsnaction : sampleTrsnactions) {
            if (sampleTrsnaction.getId() > 5) {
                break;
            }
            sampleTrsnaction.setFromEmployee(employeeToSend);
            sampleTrsnaction.setToEmployee(employeeToReceive);
            if (sampleTrsnaction.getId() % 2 == 0) {
                employeeBonus.getBonusGiven().add(sampleTrsnaction);
            } else {
                employeeBonus.getBonusReceived().add(sampleTrsnaction);
            }
        }

        return employeeBonus;

    }

    public List<EmployeeGeneralInfo> getSampleEmployeeGeneralInfoList() {
        return getEmployeeGeneralInfoFromEmployeeModule();
    }

    private List<EmployeeGeneralInfo> getEmployeeGeneralInfoFromEmployeeModule() {
        return employeeService.getSampleEmployeeGeneralInfoList();
    }

    public List<BonusTransaction> getSampleBonusTransaction() {
        List<EmployeeGeneralInfo> employeeGeneralInfos = getEmployeeGeneralInfoFromEmployeeModule();
        EmployeeGeneralInfo employeeToSend = employeeGeneralInfos.get(0);
        EmployeeGeneralInfo employeeToReceive = employeeGeneralInfos.get(1);
        List<BonusTransaction> bonusTransactions = bonusTransactionService.getSampleBonusTransaction();
        for (BonusTransaction bonusTransaction : bonusTransactions) {
            bonusTransaction.setFromEmployee(employeeToSend);
            bonusTransaction.setToEmployee(employeeToReceive);
        }
        return bonusTransactions;
    }


    public BonusTransaction createNewBonusTransaction(BonusTransaction bonusTransaction) {
        bonusTransaction.setId((int) (Math.random() * 100 + 10));
        return bonusTransaction;
    }

    public void changeBonus(BonusTransaction bonusTransaction) {
        //modify it.
    }

    public BonusTransaction voteBonus(int id, EmployeeGeneralInfo employeeToVote) {
        return getSampleBonusTransaction().get(0);
    }
}
