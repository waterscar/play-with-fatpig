package se.leandev.fatpig.bonusregistry.adaptor;

import org.junit.Test;
import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;

import java.util.List;

import static org.junit.Assert.*;

public class EmployeeBonusAdaptorTest {

    private EmployeeBonusAdaptor employeeBonusAdaptor=new EmployeeBonusAdaptor();
    @Test
    public void testGetSampleEmployeeGeneralInfoList() throws
                                                       Exception {
        List<EmployeeGeneralInfo> sampleEmployeeGeneralInfoList = employeeBonusAdaptor.getSampleEmployeeGeneralInfoList();
        assertEquals(2,sampleEmployeeGeneralInfoList.size());

    }
}