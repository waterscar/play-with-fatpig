package controllers.worktime.rest;

import play.libs.Json;
import play.mvc.*;
import se.leandev.fatpig.vacation.VacationRegistryAdaptor;
import se.leandev.fatpig.worktime.data.VacationPlan;

import java.util.List;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class VacationResource extends Controller{
    public static Result getAllVacationPlan(){
        List<VacationPlan> vacationPlans=new VacationRegistryAdaptor().getVacationPlan();
        return ok(Json.toJson(vacationPlans));
    }
}
