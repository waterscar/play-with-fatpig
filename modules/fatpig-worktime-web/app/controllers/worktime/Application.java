package controllers.worktime;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.worktimeView.index;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

}
