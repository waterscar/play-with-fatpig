package se.leandev.fatpig.registry.data;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class EmployeeService {
    public Employee getSampleEmployee(int id, String firstName, String lastName, String email) {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmail(email);
        return employee;
    }

    public List<EmployeeGeneralInfo> getSampleEmployeeGeneralInfoList() {
        EmployeeService employeeService = new EmployeeService();
        EmployeeGeneralInfo employeeToSend = employeeService.getSampleEmployee(10, "Edward", "Xie", "EX@leandev.se");
        EmployeeGeneralInfo employeeToReceive = employeeService.getSampleEmployee(11, "Bill", "Xiong", "BX@leandev.se");
        List<EmployeeGeneralInfo> employeeGeneralInfos = new ArrayList<>();
        employeeGeneralInfos.add(employeeToSend);
        employeeGeneralInfos.add(employeeToReceive);
        return employeeGeneralInfos;
    }
}
