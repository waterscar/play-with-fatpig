package se.leandev.fatpig.registry.data;

import java.util.Date;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class Project {
    private String name;
    private Date startDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
