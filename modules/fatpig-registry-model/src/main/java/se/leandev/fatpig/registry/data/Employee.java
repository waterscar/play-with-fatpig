package se.leandev.fatpig.registry.data;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class Employee extends EmployeeGeneralInfo {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Project project;


    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String getFullName() {
        return getFirstName()+" "+getLastName();
    }
}
