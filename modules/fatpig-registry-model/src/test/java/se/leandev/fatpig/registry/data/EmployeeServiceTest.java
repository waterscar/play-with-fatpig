package se.leandev.fatpig.registry.data;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeServiceTest {

    private EmployeeService employeeService;

    @Before
    public void initializeService(){
        if(employeeService==null){
            employeeService=new EmployeeService();
        }
    }

    @Test
    public void testGetSampleEmployee() throws
                                        Exception {
        Employee employee=employeeService.getSampleEmployee(12,"Edward","Xie","abc");
        assertEquals(employee.getFullName(), "Edward Xie");
    }
}