package se.leandev.fatpig.api.employee;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class EmployeeGeneralInfo {

    private String email;

    private int Id;

    private String fullName;


    public String getFullName(){
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
