package controllers.bonus;

import play.*;
import play.mvc.*;

import views.html.bonusView.index;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Bonus module is loaded!"));
    }

}
