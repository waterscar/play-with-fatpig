package controllers.bonus.rest;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.*;
import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;
import se.leandev.fatpig.bonus.data.BonusTransaction;
import se.leandev.fatpig.bonusregistry.adaptor.EmployeeBonusAdaptor;

import java.util.List;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class BonusResource extends Controller {

    private static EmployeeBonusAdaptor employeeBonusAdaptor=new EmployeeBonusAdaptor();

    public static Result getBonusTransactionsByCondition(int start, int size) {
        List<BonusTransaction> bonusTransactions = createSampleTransactions();
        int length = bonusTransactions.size();
        if (length < start) {
            return internalServerError("Wrong Param");
        }
        int end = start + size;
        if (length < end) {
            end = length;
        }
        List<BonusTransaction> bonusList = bonusTransactions.subList(start, end);
        return ok(Json.toJson(bonusList));

    }

    private static List<BonusTransaction> createSampleTransactions() {
        return employeeBonusAdaptor.getSampleBonusTransaction();
    }

    public static Result getBonusTransactionById(int id){
        for (BonusTransaction bonusTransaction : createSampleTransactions()) {
            if(bonusTransaction.getId()==id){
                return ok(Json.toJson(bonusTransaction));
            }
        }
        return notFound();
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result createBonusTransaction(){
        JsonNode node=request().body().asJson();
        BonusTransaction newTransaction=Json.fromJson(node,BonusTransaction.class);
        newTransaction=employeeBonusAdaptor.createNewBonusTransaction(newTransaction);
        return created(Json.toJson(newTransaction));
    }

    public static Result deleteBonusTransaction(int id){
        if(id<=5) {
            return ok();
        }else{
            return notFound();
        }

    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result modifyBonusTransaction(int id){
        BonusTransaction bonusTransaction=Json.fromJson(request().body().asJson(),BonusTransaction.class);

        if(id>10) {
            return notFound();
        }else if(id!=bonusTransaction.getId()){
            return badRequest();
        }else{
            employeeBonusAdaptor.changeBonus(bonusTransaction);
            return ok();
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result voteBonus(int id){
        EmployeeGeneralInfo employeeToVote=Json.fromJson(request().body().asJson(),EmployeeGeneralInfo.class);
        return ok(Json.toJson(employeeBonusAdaptor.voteBonus(id,employeeToVote)));
    }

}
