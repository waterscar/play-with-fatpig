# Bonus Web Module

This module handles logic in web of bonus, it handles function like giving bonus to others, checking my own bonus and comments.

It provide Restful service to be used by itself.

### Restful APIs

 -  [BonusResource] To provide APIs related to giving and counting bonus.


[BonusResource]:modules/fatpig-bonus-web/app/controllers/bonus/rest/BonusResource.java