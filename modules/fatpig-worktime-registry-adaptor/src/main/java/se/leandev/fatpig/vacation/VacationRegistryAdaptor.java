package se.leandev.fatpig.vacation;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;
import se.leandev.fatpig.registry.data.EmployeeService;
import se.leandev.fatpig.worktime.data.Vacation;
import se.leandev.fatpig.worktime.data.VacationPlan;
import se.leandev.fatpig.worktime.data.VacationService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Edward Xie
 *         15/2/5.
 */
public class VacationRegistryAdaptor {
    private EmployeeService employeeService=new EmployeeService();
    private VacationService vacationService=new VacationService();

    public List<VacationPlan> getVacationPlan(){
        List<VacationPlan> vacationPlans=new ArrayList<>();

        List<EmployeeGeneralInfo> sampleEmployeeGeneralInfoList = employeeService.getSampleEmployeeGeneralInfoList();
        for (EmployeeGeneralInfo employeeGeneralInfo : sampleEmployeeGeneralInfoList) {
            List<Vacation> vacations=vacationService.getVacationListForEmployeeById(employeeGeneralInfo.getId());
            VacationPlan vacationPlan=new VacationPlan();
            vacationPlan.setId(employeeGeneralInfo.getId());
            vacationPlan.setEmployee(employeeGeneralInfo);
            vacationPlan.setVacationList(vacations);
            vacationPlans.add(vacationPlan);
        }

        return vacationPlans;
    }
}
