package se.leandev.fatpig.bonus.data;

import se.leandev.fatpig.api.employee.EmployeeGeneralInfo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Edward Xie
 *         14/12/25.
 */
public class BonusTransaction {
    private int id;
    private Date createDate;
    private BigDecimal amount;
    private String description;
    private EmployeeGeneralInfo toEmployee;
    private EmployeeGeneralInfo fromEmployee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EmployeeGeneralInfo getToEmployee() {
        return toEmployee;
    }

    public void setToEmployee(EmployeeGeneralInfo toEmployee) {
        this.toEmployee = toEmployee;
    }

    public EmployeeGeneralInfo getFromEmployee() {
        return fromEmployee;
    }

    public void setFromEmployee(EmployeeGeneralInfo fromEmployee) {
        this.fromEmployee = fromEmployee;
    }
}
