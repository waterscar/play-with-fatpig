package se.leandev.fatpig.bonus.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the sample service to get sample bonus transaction
 * @author Edward Xie
 *         14/12/25.
 */
public class BonusTransactionService {
    /**
     * Get bonus transaction and return it
     * @return list of bonus transaction
     */
    public List<BonusTransaction> getSampleBonusTransaction(){
        List<BonusTransaction> bonusTransactions=new ArrayList<>();
        bonusTransactions.add(createSample(1));
        bonusTransactions.add(createSample(2));
        bonusTransactions.add(createSample(3));
        bonusTransactions.add(createSample(4));
        bonusTransactions.add(createSample(5));
        bonusTransactions.add(createSample(6));
        bonusTransactions.add(createSample(7));
        bonusTransactions.add(createSample(8));
        bonusTransactions.add(createSample(9));
        bonusTransactions.add(createSample(10));
        return bonusTransactions;
    }

    private BonusTransaction createSample(int id) {
        BonusTransaction bonusTransaction=new BonusTransaction();
        bonusTransaction.setId(id);
        bonusTransaction.setAmount(BigDecimal.valueOf(11));
        bonusTransaction.setDescription("Sample Bonus Transaction");
        return bonusTransaction;
    }
}
